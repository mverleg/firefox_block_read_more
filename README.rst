
This project has moved to https://github.com/mverleg/block_read_more
==================================================================================

I am collecting all my projects in one place, which is Github, so development continues there!

Block 'read more' (Firefox addon)
---------------------------------------

Sometimes you find the most interesting things through the various websites which suggest more of their content for you to check out. Other times you have things to do and you came to the site for some specific information. And you don't want to be distracted by '10 things you never knew about ...', you don't want to know about the history of the Qing dynasty and you especially don't want to get stuck in 'that weird part of Youtube'.

That's what this addon tries to help with. It lets you visit the content that Google (or Bing, I won't judge) found for you, without the suggestions to 'check out this amazing ...'.


Included websites
---------------------------------------

These things are blocked:

* Wikipedia: 'did you know', 'featured article' and 'today' (only news on Homepage)
* Youtube: side suggestions & main page suggestions, not the end-of-video suggestions
* Stackexchange network: hot network questions
* Facebook: just suggested groups, games, interests and lists
* Quora: several lists of related questions

(This list might not be entirely up-to-date)


Contributors
---------------------------------------

Any additional blocking rules are most welcome! The code is at https://bitbucket.org/mverleg/firefox_block_read_more


Website operators
---------------------------------------

Sorry buddies. I know you're just trying to earn some money by keeping people trapped in your little corner of the interwebs. We are also just trying to make money by doing our job without getting sidetracked constantly.


License
---------------------------------------

Revised BSD License, see LICENSE.txt. You can use the addon as you choose, without warranty. The code is also available, see Contributors.


Other addons
---------------------------------------

* Hide distracting "read more" parts of some popular sites [this one] - https://addons.mozilla.org/firefox/addon/block_read_more/
* Hide comments on some popular sites where they are notoriously unconstructive - https://addons.mozilla.org/firefox/addon/block-comments/
* Hide the registration overlay on Quora - https://addons.mozilla.org/firefox/addon/quora-unfade/

